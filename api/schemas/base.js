'use strict'
import Sequelize from 'sequelize'
export default class BaseSchema {
  constructor (modelName, schema) {
    this.schema = {
      isDeleted: { type: Sequelize.BOOLEAN, defaultValue: false },
    }
  }

  getSchema () {
    return this.schema
  }
}
