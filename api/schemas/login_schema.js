'use strict'
import {
  Model
} from 'sequelize'
import BaseSchema from './base'

const LoginSchema = (sequelize, DataTypes) => {
  const loginSchema = sequelize.define('login_schema', {
    login_schema_id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    userid: {
      type: DataTypes.STRING(500),
      allowNull: true,
      // unique: true
    },
    timestamp: {
      type: DataTypes.DATE,
      allowNull: false,
      // unique: true
    },
    action: {
      type: DataTypes.STRING(20),
      allowNull: false
    }
  });

  return loginSchema;
};
// class LoginSchema extends Model {
//   static init(sequelize, DataTypes) {
//     return super.init(Object.assign({
//       login_schema_id: {
//         type: DataTypes.UUID,
//         defaultValue: DataTypes.UUIDV4,
//         primaryKey: true
//       },
//       UserId: {
//         type: DataTypes.STRING(500),
//         allowNull: true,
//         // unique: true
//       },
//       Timestamp: {
//         type: DataTypes.DATE,
//         allowNull: false,
//         // unique: true
//       },
//       Action: {
//         type: DataTypes.STRING(20),
//         allowNull: false
//       }
//     }, new BaseSchema().getSchema()), {
//         modelName: 'login_schema',
//         tableName: 'login_schema',
//         timestamps: true,
//         sequelize
//       })
//   }
// }

export default LoginSchema
