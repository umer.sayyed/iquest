'use strict'
import { LoginSchema as schema } from '../schemas';
import Schema,{sequelizeConnection} from '../../connection';
export default class LoginHistoryModel {

  async add(req, res) {
    const loginHistory = {
      'userid': req.body.UserId,
      'timestamp': new Date(),
      'action': req.body.Action,
    }

    Schema.LoginSchema.create(loginHistory).then(() => {
      res.status(201).send({ 'message': 'loginHistory Created Successfully.' })
    }).catch((err) => {
      res.status(400).send(err)
    })
  }

  async get(req, res) {
    let id = req.params.id
    let query = `SELECT sum(case when (e.action = 'LoggedIn') then 1 else 0 end) as LoggedIn, 
    sum(case when (e.action = 'Clicked') then 1 else 0 end) as Clicked,
    sum(case when (e.Action = 'LoggedOut') then 1 else 0 end) as LoggedOut
    FROM login_schemas as e WHERE e.UserId = '${id}'`
    let result = await sequelizeConnection.query(query)
    let finalResult = result && result[0] && result[0][0] ? result[0][0] : null;
    if(finalResult)
    finalResult.UserId = id
    res.status(200).send(finalResult)
  }

}
