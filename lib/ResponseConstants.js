'use strict'

export const RESPONSE = {

    STATUS: {
        SUCCESS: "Success",
        FAILURE: "Failure"
    },

    ERROR_MESSAGE: {
        INTERNAL_ERROR: { message: "INTERNAL SERVER ERROR!!", statusCode: 500 },
        BLANK_FIELD_VALIDATION: { message: "Value should not be blank!!", statusCode: 403 },
        UNIQUE_VALIDATION: { message: "User alredy exist in the Database!!", statusCode: 403 },
        RECORD_NOT_FOUND: { message: "Record Not Found!!", statusCode: 404 }

    },

    SUCCESS_MESSAGE: {
        USER_ADDED: { message: "New Record added successfully!!", statusCode: 200 },
        USER_UPDATED: { message: "Data updated successfully!!", statusCode: 200 },
        USER_DELETED: { message: "Record(s) deleted successfully!!", statusCode: 200 },
        RECORDS_FOUND: { message: "Records Found!!", statusCode: 200 }
    }
}