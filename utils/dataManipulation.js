'use strict'
import { ResponseBody } from '../lib';
export function interwoven(data) {

    try {
        let array = data.split("");
        let result = '';
        for (let i = 0, j = array.length - 1; i < array.length; i++ , j--) {
            result += array[i] + array[j];
        }

        return new ResponseBody(200, "Success", result);

    } catch (err) {
        throw err
    }
}


export function mapping(payload, mappingData) {
    try {
        let result = {};
        for (let key in mappingData) {
            if (mappingData[key].includes('.')) {
                let finalKey = getKey(mappingData[key], payload)
                result[key] = finalKey;
            } else {
                result[key] = payload[mappingData[key]]
            }   
        }
        return new ResponseBody(200,"Success",result)
    } catch (err) {
        throw err
    }
}

function getKey(key, obj) {
    let res = key.split(".");
    let ans = obj;
    for (let i = 0; i < res.length; i++) {
        ans = ans[res[i]]
    }
    return ans;
}