
import fs from 'fs';
import { ResponseBody, RESPONSE } from '../lib'

export async function writeToFile(jsonData,res){
    await fs.writeFile('./user.json', JSON.stringify(jsonData, null, 2), 'utf-8', function (err) {
        if (err) {
          res.status(500).send(new ResponseBody(RESPONSE.ERROR_MESSAGE.INTERNAL_ERROR.statusCode, err.message))
        } else {
          res.status(200).send(new ResponseBody(RESPONSE.SUCCESS_MESSAGE.USER_ADDED.statusCode, RESPONSE.SUCCESS_MESSAGE.USER_ADDED.message, jsonData));
        }
      }
      );
}
export function checkUserIsAvailable(obj, user) {
    try {
        if (obj && user) {
            for (let i = 0; i < obj.length; i++) {
                if (obj[i].id === user.id) {
                    return true;
                }
            }
        }
    } catch (err) {
        throw err
    }
}

export function searchUsers(array, searchKey) {
    try {
        if (array && searchKey) {
            return array.filter(checkUser, searchKey)
        }
    } catch (err) {
        throw err
    }
}

export function deleteUserByID(array, id) {
    try {
        for (var i = 0; i < array.length; i++) {
            if (array[i].id === id) {
                array.splice(i, 1);
            }
        }
        return array;
    } catch (err) {
        throw err
    }
}


function checkUser(user) {
    try {
        if (user.hasOwnProperty('name'))
            return user.name.toUpperCase().includes(this.toString().toUpperCase())
    } catch (err) {
        throw err
    }
}