

import fs from 'fs';
import { ResponseBody } from '../lib'
export async function testAsync1(id) {
    try {
       let r = fs.writeFile('./error.json', JSON.stringify(JSON.parse(id)), 'utf-8')
        return new ResponseBody(200, "Success");
    } catch (error) {
        throw error
    };
}
export async function testAsync2(id) {
    try {
         fs.writeFile('./error.json', JSON.stringify(JSON.parse(id)), 'utf-8')
         return  new ResponseBody(200, "Success");
    } catch (error) {
        throw error
    };
}


export function writeToFile(data) {
    fs.writeFile('./create.json', JSON.stringify(data), 'utf-8', function (err) {
        if (err) {
            throw err
        } else {
            return 'File content saved.';
        }
    })
}