'use strict'

import Sequelize from 'sequelize'
import { POSTGRES_CONFIG } from './config/POSTGRES_CONFIG'
import  * as Schema from './api/schemas/index'
export const sequelizeConnection = new Sequelize(POSTGRES_CONFIG.DBNAME, POSTGRES_CONFIG.USERNAME, POSTGRES_CONFIG.PASSWORD, {
  host: POSTGRES_CONFIG.HOST,
  port: POSTGRES_CONFIG.PORT,
  dialect: POSTGRES_CONFIG.DIALECT,
  operatorsAliases: false,
  pool: POSTGRES_CONFIG.OPTIONS,
  logging: false
})


// Migrations.createMigrations(sequelizeConnection,Sequelize)
const schemas = {
  LoginSchema: sequelizeConnection.import('./api/schemas/login_schema'),
};

Object.values(schemas)
  .filter(model => {
    return typeof model.associate === 'function'
  })
  .forEach(model => { model.associate(models) })

//const schemas = Schema.init(sequelizeConnection,Sequelize)

// {
//   ...models,
//   sequelizeConnection
// }

sequelizeConnection.sync({alter:true})

export default schemas
