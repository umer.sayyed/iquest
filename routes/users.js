'use strict';
import { checkUserIsAvailable, searchUsers, deleteUserByID, writeToFile } from "../utils/users"
import express from 'express';
import fs from 'fs';
import jsonData from '../user.json';
import { ResponseBody, RESPONSE } from '../lib'
import { isArray } from "util";
import LoginHistoryModel from '../api/models/LoginHistoryModel'
var router = express.Router();

var loginHistoryModel = new LoginHistoryModel()
router.post('/logUserAction',loginHistoryModel.add.bind(loginHistoryModel))
router.get('/getUserAction/:id',loginHistoryModel.get.bind(loginHistoryModel))

router.post('/addUser', async function (req, res, next) {
  try {
    let data = req.body.data;
    if (req.body && data && data instanceof Object) {
      if (jsonData && isArray(jsonData)) {
        if (!checkUserIsAvailable(jsonData, data)) {
          jsonData.push(data)
          writeToFile(jsonData,res)
        } else {
          res.status(500).send(new ResponseBody(RESPONSE.ERROR_MESSAGE.UNIQUE_VALIDATION.statusCode, RESPONSE.ERROR_MESSAGE.UNIQUE_VALIDATION.message))
        }
      } else {
        let jsonData = [];
        jsonData.push(data)
        writeToFile(jsonData,res)
      }
    }
    else {
      res.status(500).send({
        statusCode: 500,
        message: "Request parameter is not an Object"
      })

    }
  } catch (err) {
    res.status(500).send(err)
  }
});

router.post('/searchUser', async function (req, res, next) {
  try {

    let searchKey =req.body.searchKey;
    if (req.body && searchKey && typeof searchKey === 'string') {
        let result = searchUsers(jsonData, searchKey)
        if (result && result.length > 0) {
          res.status(200).send(new ResponseBody(RESPONSE.SUCCESS_MESSAGE.RECORDS_FOUND.statusCode, RESPONSE.SUCCESS_MESSAGE.RECORDS_FOUND.message, result));
        } else {
          res.status(404).send(new ResponseBody(RESPONSE.ERROR_MESSAGE.RECORD_NOT_FOUND.statusCode, RESPONSE.ERROR_MESSAGE.RECORD_NOT_FOUND.message))
        }
     

    } else {
      res.status(500).send({
        statusCode: 500,
        message: "Request parameter is not a String"
      })

    }
  } catch (err) {
    res.status(500).send(err)
  }
});

router.post('/deleteUser', async function (req, res, next) {
  try {
    let id =req.body.id;
    if (req.body && id&& typeof id=== 'number') {
      let oldLength = jsonData.length;
      let result = deleteUserByID(jsonData, id);
      if (result && result.length != oldLength) {
        await fs.writeFile('./user.json', JSON.stringify(result, null, 2), 'utf-8');
        res.status(200).send(new ResponseBody(RESPONSE.SUCCESS_MESSAGE.USER_DELETED.statusCode, RESPONSE.SUCCESS_MESSAGE.USER_DELETED.message, result));
      } else {
        res.status(404).send(new ResponseBody(RESPONSE.ERROR_MESSAGE.RECORD_NOT_FOUND.statusCode, RESPONSE.ERROR_MESSAGE.RECORD_NOT_FOUND.message))
      }
    } else {
      res.status(500).send({
        statusCode: 500,
        message: "Request parameter is not a number"
      })

    }
  } catch (err) {
    res.status(500).send(err)
  }
});

module.exports = router;