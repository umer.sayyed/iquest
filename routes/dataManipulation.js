'use strict'
import express from 'express';
import { ResponseBody } from '../lib';
import { interwoven, mapping } from '../utils/dataManipulation'
var router = express.Router();

router.post('/interwoven', async (req, res, next) => {
    try {
        let response = interwoven(req.body.data);
        res.status(response.statusCode).send(response)
    } catch (err) {
        res.status(response.statusCode).send(new ResponseBody(500, "Error", err.message))
    }
})

router.post('/mapping', (req, res, next) => {
    try {
        let payload = req.body.firstParam;
        let mappingData = req.body.secondParam;
        let result = mapping(payload, mappingData)
        res.send(result)
    } catch (err) {
        res.send(new ResponseBody(500, "Success", err.message))
    }
})


module.exports = router;    