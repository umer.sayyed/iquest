import express from 'express';
import { testAsync1, testAsync2 } from '../utils/errorHandeling'
import { ResponseBody } from '../lib';

var router = express.Router();


router.post('/testAsync', async (req, res, next) => {
    let a, b;
    a = await testAsync1(req.body.data1).catch((err) => {
        res.send(new ResponseBody(500, err.message, "Error in first data"))
    })
    if (a) {
        b = await testAsync2(req.body.data2).catch((err) => {
            res.send(new ResponseBody(500, err.message, "Error in second data"))
        })
    }
    if (a && b)
        res.send("Result of Success")
})



module.exports = router;