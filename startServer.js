'use strict'

import async from 'async'
import { sequelizeConnection } from './connection'


export const startServer = () => {
  async.series([
    // Connect DB
    next => {
      console.log('[Info] Connecting to PostgreSQL...')
      sequelizeConnection.authenticate()
        .then(() =>
         { console.log('Connection to PostgreSQL has been established successfully.'); next(null) })
        .catch(err => 
          { console.error('Unable to connect to the PostgreSQL database:', err) })
    }
  ], error => {
    if (error) {
      console.error(error)
      console.error("[error] Server not started...")
      return process.exit(-1)
    }else{
      console.log('[Info] Starting Server...')
    }
  })
}


