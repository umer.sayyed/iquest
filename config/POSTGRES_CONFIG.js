'use strict'
// Sequelize Configuration to required establish connection
const POSTGRES_CONFIG = {
  DBNAME: 'postgres',
  DIALECT: 'postgres',
  HOST: 'localhost',
  PORT: '5432',
  USERNAME: 'root',
  PASSWORD: 'root',
  OPTIONS: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
    }
}

// Terminate Server if any DB Configuration is missing
Object.keys(POSTGRES_CONFIG).forEach(function (key) {
  if (!POSTGRES_CONFIG[key]) {
    console.error("[Error] Missing POSTGRES Config:", key)
    return process.exit(1)
  }
})

export { POSTGRES_CONFIG }
